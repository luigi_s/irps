#include <MKL25Z4.H>
#include <stdbool.h>
#include "gpio_defs.h"
#include "adc_defs.h"
#include "SysTick.h"
#include <stdlib.h>
#include <Math.h>

/* ------------------------------------------
       ECS727 Lab for week 4
	ADC and Proximity Sensor
  -------------------------------------------- */

/*  -------------------------------------------
      Demonstration of simple ADC

    1. The DAC input is ADC0_SE8 (ALT0 of PTB0), 
		     which is pin 2 on FRDM-KL25Z J10
	  2. Measures voltage every cycle; average 5 measurements
  	3. Use RGB LED to display out as follows:
		     Voltage         LED
		       0-33%           red
					 34-66%          green
		       >=67%           blue
    ------------------------------------------- */


/* ------------------------------------------
   Initialise on board LEDs
	    Configuration steps
	      1. Enable pins as GPIO ports
	      2. Set GPIO direction to output
	      3. Ensure LEDs are off

   Red LED connected to Port B (PTB), bit 18 (RED_LED_POS)
   Green LED connected to Port B (PTB), bit 19 (GREEN_LED_POS)
   Blue LED connected to Port D (PTD), bit 1 (BLUE_LED_POS)
   Active-Low outputs: Write a 0 to turn on an LED
   ------------------------------------------ */
void Init_LED() {

  // Make 3 pins GPIO
	PORTB->PCR[RED_LED_POS] &= ~PORT_PCR_MUX_MASK;          
	PORTB->PCR[RED_LED_POS] |= PORT_PCR_MUX(1);          
	PORTB->PCR[GREEN_LED_POS] &= ~PORT_PCR_MUX_MASK;          
	PORTB->PCR[GREEN_LED_POS] |= PORT_PCR_MUX(1);          
	PORTD->PCR[BLUE_LED_POS] &= ~PORT_PCR_MUX_MASK;          
	PORTD->PCR[BLUE_LED_POS] |= PORT_PCR_MUX(1);          
	// IR
	PORTE->PCR[IR_DIODE] &= ~PORT_PCR_MUX_MASK;          
	PORTE->PCR[IR_DIODE] |= PORT_PCR_MUX(1);          

	// Set ports to outputs
	PTB->PDDR |= MASK(RED_LED_POS) | MASK(GREEN_LED_POS);
	PTD->PDDR |= MASK(BLUE_LED_POS);
	// IR 
	PTE->PDDR |= MASK(IR_DIODE);	

  // Turn off LEDs
	PTB->PSOR = MASK(RED_LED_POS) | MASK(GREEN_LED_POS);
	PTD->PSOR = MASK(BLUE_LED_POS);
	// Turn on IR LED
	PTE->PCOR = MASK(IR_DIODE);
	
}


/*  -----------------------------------------
     Task 2: DisplayUsingLED
        voltageState    LED
           0              red
           1              green
           2              blue
    -----------------------------------------   */
volatile int voltageState ;

//void task2DisplayUsingLED(void)
//{
//	if (voltageState == 0) {
//	   // set just red on
//		 PTB->PCOR = MASK(RED_LED_POS) ;
//		 PTB->PSOR = MASK(GREEN_LED_POS) ;
//		 PTD->PSOR = MASK(BLUE_LED_POS) ;
//	} else if (voltageState == 1) {
//		 // set just green on
//		 PTB->PCOR = MASK(GREEN_LED_POS) ;
//		 PTB->PSOR = MASK(RED_LED_POS) ;
//		 PTD->PSOR = MASK(BLUE_LED_POS) ;
//	} else {
//		 // set just blue on
//		 PTB->PSOR = MASK(RED_LED_POS) | MASK(GREEN_LED_POS) ;
//		 PTD->PCOR = MASK(BLUE_LED_POS) ;
//	}
//}

/*  -----------------------------------------
     Task 1: MeasureVoltage
        voltageState    LED
           0              red
           1              green
           2              blue
    -----------------------------------------   */
// declare volatile to ensure changes seen in debugger
volatile int measured_voltage ;  // scaled value
//volatile unsigned res ;        // raw value 

//void task1MeasureVoltage(void) {
//	// take 5 voltage readings
//	int i ;
//	res = 0 ;
//	for (i = 0; i < 5; i++) { 
//	  	// measure the voltage
//    res = res + MeasureVoltage() ;
//	}
//  res = res / 5 ;	
//	
//	// Set the voltage state
//	if (res < ADCLOW) voltageState = 0 ;
//	else if (res < ADCMEDIUM) voltageState = 1 ;
//	else voltageState = 2 ;

//	// Scale to an actual voltage, assuming VREF accurate
//	// This step is not essential - but value can be observed in debugger
//  measured_voltage = scaleVoltage(res) ;
//	// Note conversion from unsigned to signed is ok as 
//	//   unsigned value is 12 bits	
//}


int measure_counter = 1;
volatile int ms = 0;
int speed_idx = 0;
int sg_idx = 1;

//int r[1000];
//int v_on[1000];
int sec[1000];
int gap[1000];
int spd[1000];




int READING_STATE = PREPARE_READING;
int V_OFF_READING = 0;
int V_ON_READING = 0;
int READINGS_DONE = 0;
unsigned ON_OFF_GAP = 0;



void readVoltage(void) {
	// take 5 voltage readings
	int i ;

		res = 0 ;
		for (i = 0; i < 5; i++) { 
			// measure the voltage
			res = res + MeasureVoltage() ;
		}
			
		res = res / 5 ;	

		// Scale to an actual voltage, assuming VREF accurate
		// This step is not essential - but value can be observed in debugger
		measured_voltage = scaleVoltage(res) ;
		// Note conversion from unsigned to signed is ok as 
		// unsigned value is 12 bits
}

//double theta[3] = {23.907808, 4.859851, 4.859861 } ; // gradient descent normalised. Input must be normalised
 
double theta[6] = {
	139.261118 
, 8.414290 
, -33.489225 
, -27.742599 
, -13.325851 
, -3.117010}; 


double mu[5] = {
  10017.077647
, 0.000027
, 2.414300
, -9.139099
, 1.829500};


double sigma[5] = {
  4562.233637
, 0.000032
, 1.402600
, 0.351578
, 3.553800};

/*
X1 = x1;
X2 = X1.^((-(log(X1))));
X3 = (log(X1)).^((-(log(X1))));
X4 = -log(X1);
X5 = X1.^3;
*/

double input[6];

void prepareInput(int _input)
{
	int i;
	for (i = 0; i < 5; i++){
		switch (i)
		{
			case 0:{
				input[1] = _input;
				input[1] = (input[1] - mu[i]) / sigma[i];
				break;
			}
			case 1:{
				input[2] = (pow(_input, -(log(_input))))*(pow(10, 30)) ;
				input[2] = (input[2] - mu[i]) / sigma[i];
				break;
			}
			case 2:{
				input[3] = (pow(log(_input), -(log(_input))))*(pow(10,9));
				input[3] = (input[3] - mu[i]) / sigma[i];
				break;
			}
			case 3:{
				input[4] = -log(_input);
				input[4] = (input[4] - mu[i]) / sigma[i];
				break;
			}
			case 4:{
				input[5] = (pow(_input, 3))*(pow(10, -12));
				input[5] = (input[5] - mu[i]) / sigma[i];
				break;
			}
		}
	}
	input[0] = 1;
}



volatile double distance;

void measureDistance(void)
{
	int i;
	double sum = 0;
	for (i = 0; i < 6; i++){
			sum = sum + (theta[i] * input[i]);
	}
	distance = 275.0237 - sum;
}


void task2SetVoltages(void)
{	
	volatile int LEVEL = 100 - ( 100 * ON_OFF_GAP ) / V_OFF_READING;
	
	if ( READING_STATE == READY )
	{
		if(ON_OFF_GAP > 6926){
			prepareInput(ON_OFF_GAP);
			measureDistance();
		}
		else
				distance = -1;
		
		if 			(distance < 60 ) // percentage are normalised 
			voltageState = 0;
		else if (distance < 100 )
			voltageState = 1;
		else if (distance < 150 )
			voltageState = 2;
		else if (distance < 200 )
			voltageState = 3;
		else if (distance < 250 )
			voltageState = 4;
		else voltageState = 5;
	}
}


void task3DisplayUsingLED(void)
{
	if (voltageState == 0) {
	   // set white
		 PTB->PCOR = MASK(RED_LED_POS) ;
		 PTB->PCOR = MASK(GREEN_LED_POS) ;
		 PTD->PCOR = MASK(BLUE_LED_POS) ;
	} else if (voltageState == 1) {
		 // set cyano 
		 PTB->PCOR = MASK(GREEN_LED_POS) ;
		 PTB->PSOR = MASK(RED_LED_POS) ;
		 PTD->PCOR = MASK(BLUE_LED_POS) ;
	} else if (voltageState == 2) {
		 // set orange
		 PTB->PCOR = MASK(GREEN_LED_POS) ;
		 PTB->PCOR = MASK(RED_LED_POS) ;
		 PTD->PSOR = MASK(BLUE_LED_POS) ;
	} else if (voltageState == 3) {
		 // set green
		 PTB->PCOR = MASK(GREEN_LED_POS) ;
		 PTB->PSOR = MASK(RED_LED_POS) ;
		 PTD->PSOR = MASK(BLUE_LED_POS) ;
	} else if (voltageState == 4) {
		 // set purple
		 PTB->PSOR = MASK(GREEN_LED_POS) ;
		 PTB->PCOR = MASK(RED_LED_POS) ;
		 PTD->PCOR = MASK(BLUE_LED_POS) ;
	} else  {
		 // blue
		 PTB->PSOR = MASK(GREEN_LED_POS) ;
		 PTB->PSOR = MASK(RED_LED_POS) ;
		 PTD->PCOR = MASK(BLUE_LED_POS) ;
	}
}

int EMITTER_STATE = ON;

//toggle IR LED
void toggleIRLed(void)
{
	if (EMITTER_STATE == ON)
	{
		PTE->PSOR = MASK(IR_DIODE);
		EMITTER_STATE = OFF;
	}		
	else
	{
		PTE->PCOR = MASK(IR_DIODE);
		EMITTER_STATE = ON;
	}
}

double speed;

void task1MeasureOnOffGap(void)
{
	switch (READING_STATE)
	{
		//prepare reading
		case PREPARE_READING:
		{
			if(READINGS_DONE != 2)
			{
				toggleIRLed();
				READING_STATE = READING;
				// system will run another cycle
			}
			else 
			{
				if (MEASURING_MODE)
				{
					ON_OFF_GAP = V_OFF_READING - V_ON_READING;
					if(sg_idx < 1001){
						if(measure_counter>0){
							//r[measure_counter - 1] = res;
							//v_on[measure_counter -1] = V_ON_READING;
							
							if(ON_OFF_GAP > 30900)
							{
								if(ms>0){
									int i;
									for(i = 0; i < ms; i++)
									{
										if(speed_idx<1001){
											spd[speed_idx] = 290/ms;
											speed = 290/ms;
											speed_idx++;
										}
									}
								}
								PTB->PSOR = MASK(GREEN_LED_POS) ;
								PTB->PSOR = MASK(RED_LED_POS) ;
								PTD->PSOR = MASK(BLUE_LED_POS) ;
								measure_counter++;
								ms = 0;
								waitSysTickCounter(2000) ;
							}
							
							
							if(ON_OFF_GAP >= 6980){
								
								if(ms==1)
								{
									PTB->PSOR = MASK(GREEN_LED_POS) ;
									PTB->PSOR = MASK(RED_LED_POS) ;
									PTD->PSOR = MASK(BLUE_LED_POS) ;
								}
								ms++;
								sec[sg_idx-1] = ms;
								gap[sg_idx-1] = ON_OFF_GAP; //no spece enough, it is derived along with the dumping process
								sg_idx++;
							}
						}
					}
					else
					{
						// code executed if arrays are filled in MEASURING_MODE active
						// If some extra action is required after all the values have been recorded.
						// i.e. if RESET measure_counter = 0 here if want to restart measuring all over cycling.
						
						
					}
					READINGS_DONE = 0;
					READING_STATE = READY;
				}
				else
				{
					// Original block of code inaltered after MEASURING_MODE was introduced.
					ON_OFF_GAP = V_OFF_READING - V_ON_READING;
					READINGS_DONE = 0;
					READING_STATE = READY;
				}
				
			}
			break;
		}
		case READING:
		{
			readVoltage();
			
			if(EMITTER_STATE == ON)
				V_ON_READING = measured_voltage;
			else
				V_OFF_READING = measured_voltage;
			
			READING_STATE = PREPARE_READING;
			READINGS_DONE++;
			break;
		}
		case READY:
		{
			READING_STATE = PREPARE_READING;
			break;
		}
		default: break;
	}
	
}



/*----------------------------------------------------------------------------
  MAIN function
 *----------------------------------------------------------------------------*/


int main (void) {


	// Enable clock to ports B and D
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK | SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK ;

	Init_LED() ; // initialise LED
	Init_ADC() ; // Initialise ADC
	Init_SysTick(1000) ; // initialse SysTick every 1ms
	waitSysTickCounter(50) ;

	while (1) {		
		
		
		task1MeasureOnOffGap();
	
		task2SetVoltages ();
		
		task3DisplayUsingLED();
		
		waitSysTickCounter(5) ;  

		
	}
}


