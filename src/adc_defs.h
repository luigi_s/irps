#ifndef ADC_DEFS_H
#define ADC_DEFS_H


// Freedom KL25Z ADC Channel
#define ADC_CHANNEL (8)	// on port B
#define VREF (33000) // reference voltage: 1 is 0.1 millivolts
#define ADCMAX (4095)    // maximum for a 12 bit conversion
#define ADCMEDIUM (2730) // 2/3 of 12 bit range
#define ADCLOW (1365)    // 1/3 of 12 bit range


#define _20 (819)    	// 20%
#define _40 (2730) 		// 40%
#define _60 (2457)    // 60%
#define _80 (3276)    // 80%
#define _100 (4095) 	// 100%







// distance constants
#define _21CM (25263)
#define _17CM (25070)
#define _13CM (24554)
#define _8CM (23313)
#define _4CM (15682)


// External variables
extern volatile unsigned res ;            // raw value 
 
// Function prototypes 
unsigned MeasureVoltage(void) ;
void Init_ADC(void) ;
int scaleVoltage(int rawADCVal) ;

#endif
