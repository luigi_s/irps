#ifndef GPIO_DEFS_H
#define GPIO_DEFS_H

#define MASK(x) (1UL << (x))

// Freedom KL25Z LEDs
#define RED_LED_POS (18)		// on port B
#define GREEN_LED_POS (19)	// on port B
#define BLUE_LED_POS (1)		// on port D
#define IR_DIODE (4)


#define OFF (0)
#define ON (1)

#define PREPARE_READING (0)
#define READING (1)
#define READY (2)

#define MEASURING_MODE 0

#endif
// *******************************ARM University Program Copyright � ARM Ltd 2013*************************************   
