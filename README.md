# README #

This is the available code of IR proximity sensor for distances estimation via polynomial regression executed in Octave environment.

### What is this repository for? ###

* Make easy to get and public the code
* Version 1.0 Alpha

### How do I get set up? ###

* Look at the Octave directory for all you need about the regression
* Look at the root directory for the C source code


### Two main steps ###

* Controlling the hardware
* Build a model for the input voltage measured by the sensor