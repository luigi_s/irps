function [X_norm, mu, sigma] = featureNormalize(X)

    X_norm = X;

    mu = zeros(1, size(X, 2));

    sigma = zeros(1, size(X, 2)); 

    mu = mean(X);

    sigma = std(X);      

    m = size(X,1);      

    A = repmat(mu,m,1);      

    X_norm = X_norm - A;      

    A = repmat(sigma,m,1);      

    X_norm =X_norm./A;

end