function [a b] = expfit2(x,y)
%% nlinfit example
% EF 230, Fall, 2009
% The example demonstrates how nlinfit can be used to fit an arbitrary
% equation to a give set of data. The general idea is that you define
% the target function with all of its coefficients and constants as variables
% and allow nlinfit to determine the set of numbers for these values that
% will provide the best fit to the given data.

% define the function that we want to fit to
funky = @(b,x)b(1)*exp(b(2)*x);

% use nlinfit to find the coefficients for the function that
% best represent the data
beta=nlinfit(x,y,funky,[1 1]);

% generate points on the best fit curve and plot it
yfit=funky(beta,x);
a = beta(1);
b = beta(2);

% show the input data as points
plot(x,y,'*')
hold on

% show results
plot(x,yfit,'r');
txt = sprintf('Fitted equation %.2f*exp(%.2fx)',a,b);
legend('Data Points',txt);
