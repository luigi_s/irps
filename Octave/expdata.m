function y = expdata(x,a,b)
%% Simulate experimental data that follows an exponential curve a*exp(b*x)
% idealized data
yideal=a*exp(b*x);
% add random noise to the y values
y=awgn(yideal,.5);
return;
