% Spline curve
% A very simple example of using spline to draw a curve
% through a set of points
% this is NOT the same as a best fit curve,
% but it might be useful for a roller coaster!
clear all; close all; clc;
x = 0:10; % simple set of x values
y = randi(20,size(x)); % random y values for each x value
xx = linspace(0,10,1000); % many x values for plotting
yy = spline(x,y,xx); % generate y values based on spline interpolation
plot(x,y,'ro'); % red data points
hold on;
plot(xx,yy,'b-'); % blue spline curve
legend('data points','spline curve');
title('A spline is NOT a best fit curve');